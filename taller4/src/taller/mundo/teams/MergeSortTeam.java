package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 if(lista.length < 2)
    	 {
    		 return lista;
    	 }
    	 int mid = lista.length/2;
    	 Comparable[] izq = new Comparable[mid];
    	 for(int i = 0; i<mid; i++)
    	 {
    		 izq[i] = lista[i];
    	 }
    	 Comparable[] der = new Comparable[lista.length - mid];
    	 for(int i = 0; i<der.length; i++)
    	 {
    		 der[i] = lista[mid+i];
    	 }
    	 izq = merge_sort(izq, orden);
    	 der = merge_sort(der, orden);
    	 
    	 lista = merge(izq,der, orden);
    	 
    	 	
    	 
    	 return lista;
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {
    	 Comparable[] rta = new Comparable[izquierda.length + derecha.length];
    	 int i = 0;
    	 int j = 0;
    	 for(int k = 0; k<rta.length; k++)
    	 {
    		 if(i == izquierda.length)
    		 {
    			 rta[k] = derecha[j++];
    		 }
    		 else if(j == derecha.length)
    		 {
    			 rta[k] = izquierda[i++];
    		 }
    		 else if(!hEH(izquierda[i], derecha[j], orden))
    		 {
    			 rta[k] = izquierda[i++];
    		 }
    		 else
    		 {
    			 rta[k] = derecha[j++];
    		 }
    	 }
    	 
    	   	 
    	 
    	 return rta;
     }

     private static boolean hEH(Comparable a, Comparable b, TipoOrdenamiento orden)
     {
           int stat = a.compareTo(b);
           boolean val = stat > 0 && orden == TipoOrdenamiento.ASCENDENTE;
           val = val || stat < 0 && orden == TipoOrdenamiento.DESCENDENTE;
           return val;
     }

}
