package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;


public class JavierSortTeam extends AlgorithmTeam
{
     public JavierSortTeam()
     {
          super("Javier sort (-)");
          userDefined = false;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          return javierSort(list, orden);
     }

     /**
     Ordena un arreglo de enteros, usando Javier Sort.
     @param arr Arreglo de enteros.
     **/
     private Comparable[] javierSort(Comparable[] arr, TipoOrdenamiento orden)
     {
    	 if(arr.length < 26000)
    	 {
    		 return new BubbleSortTeam().sort(arr, orden);
    	 }
    	 else
    	 {
    		 return new MergeSortTeam().sort(arr, orden);
    	 }
     }
    }


